#include<iostream>
#include<cstdlib>
#include<stdio.h>

using namespace std;

class List
{
	private:
		typedef struct node
		{
			int number;
			string title;
			string singer;
			node* next;
			
		}*nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;
	
	public:
		List();
		void Push(string addTitle, string addSinger);
		void Pop();
		void Display();
		string DisplayTop();
};

List::List()
{
	head=NULL;
	curr=NULL;
	temp=NULL;
	top = NULL;
}
void List::Push(string addTitle, string addSinger)
{
	nodePtr x = new node;
	x->title = addTitle;
	x->singer = addSinger;
	x->next = NULL;
	if(head!=NULL)
	{
		curr=head;
		while(curr->next!=NULL)
		{
			curr = curr->next;
		}
		curr->next = x;
		x->number=top->number+1;
		curr=curr->next;
	}
	else
	{
		head=x;
		x->number=1;
	}
	top = x;
}

void List::Pop()
{
	curr = head;
	temp = head;
	if (head==NULL)
	{
		cout<<"Nothing To Remove"<<endl;
	}
	else if(curr->next==NULL)
	{
		head=NULL;
		cout<<"Empty Stack\n";
	}
	else
	{
		while(curr->next!=NULL)
		{
			temp = curr;
			curr = curr->next;
		}
		top = temp;
		top->next=NULL;
		
	}
}

void List::Display()
{
	if(head!=NULL)
	{
		for(int y = top->number;y>0;y--)
		{
			curr=head;
			while(curr!=NULL&&curr->number!=y)
			{
				curr = curr->next;
			}
			cout << curr->number << ".Music Title: " << curr->title <<" by: "<<curr->singer<< endl<<endl;
		}
	}
	else
	{
		cout<<"Nothing To Display\n";
	}
}


List list;

void switcher()
{
	cout<<"1. ADD OR PUSH \n";
	cout<<"2. REMOVE OR POP \n";
	cout<<"3. DISPLAY \n";
	
	cout<<"Input choice: ";
	char z;
	cin>>z;
	cin.ignore(1,'\n');
	switch (z)
	{
		case '1':
		{
			string addTitle, addSinger;
			cout<<"Input Title: ";
			getline(cin,addTitle);
			cout<<"Input Singer: ";
			getline(cin,addSinger);
			list.Push(addTitle, addSinger);
			cout<<"Already Saved!\n";
			break;
		}
		case '2':
		{
			list.Pop();
			break;
		}
		case '3':
		{
			list.Display();
			break;
		}
		default:
		{
			cout<<"Invalid Input!\n";
			break;
		}
	}
	switcher();
}


int main()
{
	cout<<"WELCOME TO MUSIC STACK\n\n";
	switcher();
}